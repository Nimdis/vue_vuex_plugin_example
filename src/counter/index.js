import { mapActions } from 'vuex'
import template from './index.jade'

export default {
  template: template(),
  methods: mapActions([
    'undo',
    'redo',
    'incrementAsync'
  ])
}
