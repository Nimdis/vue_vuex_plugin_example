import Vue from 'vue'
import Vuex from 'vuex'
import { merge } from 'lodash'

Vue.use(Vuex)

const ls = window.localStorage

const initialState = {
  count: 0
}

const mutations = {
  increment(state) {
    state.count += 1
  },
  decrement(state) {
    state.count -= 1
  },
}

const actions = {
  incrementAsync({ commit }) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit('increment')
        resolve()
      }, 1000)
    })
  },
  undo(state) {
    console.log(state)
  },
  redo(state) {
    console.log(state)
  }
}

const plugins = [store => {
  store.subscribe((mutation, { count }) => {
    if (mutation.type === 'increment') {
      ls.setItem('versions', JSON.stringify({ a: count }))
    }
  })
}]

export default new Vuex.Store({
  state: initialState,
  actions,
  mutations,
  plugins
})
