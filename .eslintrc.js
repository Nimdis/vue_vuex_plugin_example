module.exports = {
  root: true,
  extends: 'standard',
  extends: 'airbnb-base',
  plugins: [
    'html'
  ],
  'rules': {
    'arrow-parens': ['error', 'as-needed'],
    'import/no-unresolved': 0,
    'no-param-reassign': ["error", { "props": false  }],
    'max-len': ["warn", 80],
    'spaced-comment': ["warn"],
    'semi': 0,
    'no-undef': ["warn"],
    'comma-dangle': 0,
    'no-unused-vars': 1,
    'no-new': 0,
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
